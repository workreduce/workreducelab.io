                enter.append("path")   
                .attr("class", "trace")   
                .attr("id", (d,i)=>{return i})       
                .attr("stroke", '#cccccc')
                .style("stroke-width", 2)
                .style("stroke-opacity", 0.5)
                .style("fill", "none")
                .attr("d", (d)=>{
                    return path(d)})
                .each(function(d) { d.totalLength = this.getTotalLength(); })
                .attr("stroke-dasharray", (d,i)=>{
                    return d.totalLength + " " + d.totalLength;
                })
                .attr("stroke-dashoffset", function(d) { return d.totalLength; })
                .transition()
                    .duration(1200)
                    //.ease("linear")
                    .attr("stroke-dashoffset", (d)=>{
                        var value=d.totalLength;
                        if(d.visible){value = 0};
                        return value;
                    })

            },

            update => {
                //console.log("update")
                update.attr("d", (d)=>{
                        return path(d)})
                    .each(function(d) { d.totalLength = this.getTotalLength(); })
                    .attr("stroke-dasharray", (d,i)=>{
                        return d.totalLength + " " + d.totalLength;
                    })
                    //.attr("stroke-dashoffset", function(d) { return d.totalLength; })
                    .transition()
                        .duration(1200)
                        //.ease("linear")
                        .attr("stroke-dashoffset", (d)=>{
                        var value=d.totalLength;
                        if(d.visible){value = 0};
                        return value;
                    })
            }//,